# Installation
> `npm install --save @types/escodegen`

# Summary
This package contains type definitions for escodegen (https://github.com/estools/escodegen).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/escodegen.

### Additional Details
 * Last updated: Tue, 06 Jul 2021 19:03:39 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Simon de Lang](https://github.com/simondel).
